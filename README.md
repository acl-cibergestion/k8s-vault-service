# ACL-Cibergestion
## _k8s-vault-service_

k8s-vault-service es el motor de Vault, donde estarán almacenadas las propiedades de entorno que usarán los MicroServicios desarrollados con SpringBoot. 

## Comenzando
Estas instrucciones permitirán instalar Vault en Kubernetes. 
Documentación completa: 
#####  https://devopscube.com/vault-in-kubernetes/

- Vault RBAC Setup: El servidor Vault requiere permisos adicionales de Kubernetes para realizar sus operaciones. 
```sh
kubectl apply -f 01-rbac.yaml
```
- Creación de mapas de configuración de Vault:
```sh
kubectl apply -f 02-configmap.yaml
```
- Implementar servicios de Vault
```sh
kubectl apply -f 03-services.yaml
```
- Implementar Vault StatefulSet, primero editar las lineas 61 y 63 con las Ips que el Kubernetes asignó.
```sh
- name: VAULT_ADDR
   value: "http://127.0.0.1:8200"
 - name: VAULT_API_ADDR
    value: "http://$(POD_IP):8200"
```
Posteriormente ejecutar:
```sh
kubectl apply -f 04-statefulset.yaml
```

## Inicializar Vault
```sh
kubectl exec vault-0 -- vault operator init -key-shares=1 -key-threshold=1 -format=json > keys.json

```
Esto generará el Token de Autenticacion y las claves para crear el token (Seal/Unseal)

Para los diferentes ambientes se creo keys-dev.json y key-qa.json

## Unseal Vault
Unseal es el estado en el que la bóveda puede construir las claves necesarias para descifrar los datos almacenados en su interior.
(Revisar en el fichero que se creo, key.json y extraer los valores para sustituir)

```sh
kubectl exec vault-0 -- vault operator unseal $unseal_keys_b64[]
```
## Iniciar sesión y acceder a la interfaz de usuario de Vault

Login en vault
```sh
 kubectl exec vault-0 -- vault login $root_token
```


## Exponer el pod del vault, para acceder a el por la ui
vault-0 -> pod de vault que se requiere exponer
-n seguridad -> namespace donde esta ese pod
--name vault-service -> nombre del servicio que queremos exponer
--port 8200 -> puerto para llegar al pod

```sh
 kubectl expose pod vault-0 -n seguridad --name vault-service --type LoadBalancer --port 8200 --protocol TCP
```

##Estos comandos que hay aca, es para crear las propiedades del vault, si no se quiere entrar por la ventana web, que se acaba d crear, en el punto d arriba.
Crear secrets
```sh
vault secrets enable -version=2 -path="nuestro_microservicio" kv
```
Create propiedades clave-valor
```sh
vault kv put secret/nuestro_microservicio alguna.propiedad=akgun_valor
```
Consultar propiedades clave-valor
```sh
vault kv get kv/nuestro_microservicio
```

## Notas
Para eliminar el servidor Vault creado anteriormente e instalar otro Vault, 
es necesario eliminar todas las dependencias anteriormente creadas, incluido el volumen de persistencia, ejemplos:
```sh
kubectl get pvc
```

```sh
kubectl delete pvc/data-vault-0
```





